1. create the `self.pem` file and put it inside the `noVNC` directory 
  * `openssl req -new -x509 -days 365 -nodes -out self.pem -keyout self.pem`
  * `https://github.com/kanaka/websockify/wiki/Encrypted-Connections`

2. run `docker-compose build`
3. run `docker-compose up`
4. In your browser `https://localhost:6080/vnc.html?host=localhost&port=6080&autoconnect=1&password=secret&encrypt=1`
5. Do Stuff
6. CTRL + C when done
7. `docker-compose downs